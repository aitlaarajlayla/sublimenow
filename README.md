# sublimeNow
Sync files between serviceNow instance and sublime text editor.
This repository is based on the work done in the dynamicdan/sn-filesync repository.
For more details visit the original  [repository](https://github.com/dynamicdan/sn-filesync).

In this repo I implemented conflicts management when pushing or pulling a file to or from serviceNow instance. see  [conflicts management](#conflicts-management) part for more details.

## Getting started
### Installation
- Download the latest version of node at [nodejs.org](https://nodejs.org/en/download/).
- Download or clone the latest version of this repository.
- Install dependencies by running the command:

```sh
$ npm install
```
    
- Confirm setup is complete by running the command: 

```sh
$ node bin/app.js --help
```
    
If all is setup correctly, you will see a list of possible options to use.
    
### Configuration
- Create a folder where records will be sync, "sync_folder/instance_name"
- In the config file "app.config.json" enter your credentials (username and password).
- Run the command to setup connection to the instance

```sh
$ node bin/app.js --config app.config.json
```
    
when this command is processed, the credentials entered in the config file are replaced with a single authentication crypted property (the password is no more visible :) )
    
### Possible Operations

#### Search
What to do if I want to find all scripts I have already created or updated in serviceNow instance?

In the config file (app.config.json) in the search property add a new one called "mine" for example:

```sh 
    "search": {
        "mine": {
            "query": "active=true^sys_updated_by=javascript:gs.getUserName()^ORsys_created_by=javascript:gs.getUserName()^ORDERBYDESCsys_updated_on",
            "records_per_search": "100"
        }
    }
```

This should fetch all script created or updated by the current user. For that query it will limit the search on 100 scripts since it's specified via the property "records_per_search".

If you want to download those script you add the property ("download": true) to the "mine" search.

If you want to limit the search only to a specific type of script for example "ui script", you must add a new property ("table": "sys_ui_script").
"sys_ui_script" is the name of servoceNow table where ui scripts are stored.

Once the config is settup correctly, the search can be performed using the command:

```sh
    $node bin/app.js --config app.config.json --search mine --download
```

#### Pull
To pull a script from serviceNow instance you have to ensure that this file is already syncronised via a search query.
Then you just need to run the following command:

```sh
   $ node bin/app.js --config app.config.json --pull "<script_folder>/<script_name.extension>"
```

#### Push
To push a script to serviceNow instance you have to ensure that this file is already syncronised via a search query.
Then you just need to run the following command:

```sh
    $ node bin/app.js --config app.config.json --push "<script_folder>/<script_name.extension>"
```

### Conflicts management

Conflicts are detected when you want to pull/push a script that was changed in the serviceNow instance or locally in sublimeText. So conflicts are handled in pull and push operations.

When you want to perform a pull operation on a file that was changed in serviceNow instance for example, the command 'node bin/app.js --config app.config.json --pull "ui_scripts/testSublimeTextConnection.js"' will result in:

[![Conflicts log in the terminal](https://bitbucket.org/aitlaarajlayla/sublimenow/raw/master/resources/pull-conflicts.png)]("")

If you choose The option "Overwrite local file", the local file will be overwritten by the content of the file on serviceNow instance.

If you choose The option "Resolve conflicts", a log of conflicts will be shown in the terminal giving you an idea about what was added and what was removed. 
A system notification is also showen to remind you there is conflicts in the specefied file.

[![Conflicts log in the terminal](https://bitbucket.org/aitlaarajlayla/sublimenow/raw/master/resources/conflicts.png)]("")

When you open that file in your text editor SublimeText, you will find there information about what was added and what was removed.

[![Conflicts log in the terminal](https://bitbucket.org/aitlaarajlayla/sublimenow/raw/master/resources/sublime-conflicts.png)]("")

All you need now is resolve your conflicts and push a clean version of the file to your instance.

Conflicts are also handled in the push operation the same way as for the pull operation. The defference is that since we are in a push operation when conflicts are detected we are invited to either overwrite serviceNow script or resolve conflicts, see image below:

[![Conflicts log in the terminal](https://bitbucket.org/aitlaarajlayla/sublimenow/raw/master/resources/push-conflicts.png)]("")

### Conclusion

Actually conflicts are handled for push/pull operation only when we specify the path of the file:

```sh
    $ node bin/app.js --config app.config.json --pull "ui_scripts/testSublimeTextConnection.js"
    $ node bin/app.js --config app.config.json --push "ui_scripts/testSublimeTextConnection.js"
```

Impovements needed:

- Take into account when we pull a file by specifying its sys_id or its table.
- Auto merge.