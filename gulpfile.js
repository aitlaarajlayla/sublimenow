var gulp = require('gulp'),
    // exec = require('gulp-exec'),
    exec = require('child_process').exec,
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    runSequence = require('run-sequence'),
    jscs = require('gulp-jscs');

var options = require('minimist')(process.argv.slice(2));

gulp.task('default', function() {
    //run a sequence of tasks
});

gulp.task('jshint-client', function () {
	return gulp.src(['sync_folder/*/ui_scripts/*.js', 'sync_folder/*/business_rules/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('jshint-server', function () {
	return gulp.src(['sync_folder/*/script_includes/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter(stylish));
});

gulp.task('jshint', function (callback) {
	/**
	 * This will run in this order: 
	 * jshint-client 
	 * jshint-client and jshint-server in parallel 
	 * jshint-server 
	 * Finally call the callback function
	 */
  	runSequence('jshint-client',
				//['jshint-client', 'jshint-server'],
				'jshint-server',
				callback);
});

gulp.task('jscsCheck', function () {
    return gulp.src('sync_folder/*/ui_scripts/testSublimeTextConnection.js')
        .pipe(jscs({fix: true}))
        .pipe(jscs.reporter())
        .pipe(jscs.reporter('fail'))
        .pipe(gulp.dest('src'));
});

gulp.task('search', function (cb) {
	var type = options.type ? options.type : '',
		download = options.download ? '--download' : '',
		search = "node bin/app.js --config app.config.json --search " + options.type + ' ' + download;

	exec(search, function (err, stdout, stderr) {
		// debugger;
		console.log(stdout);
		//console.log(stderr);
		//cb(err);
	});
	// var download = options.download ? "--download" : "";
	
	/*if (options.type) {
		console.log(options.type);
		shell.task( [
        	"node bin/app.js --config app.config.json --search" + options.type + download
    	])
	}*/
});